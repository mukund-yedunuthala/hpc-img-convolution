.. Image convolution using MPI documentation master file, created by
   sphinx-quickstart on Thu Jul  6 18:07:17 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Image convolution using MPI
===========================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Table of Contents
^^^^^^^^^^^^^^^^^

.. toctree:: 

   self
   api/index