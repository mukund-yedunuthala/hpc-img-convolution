.. _api_seq_convolutions:

Sequential Convolution
======================

.. doxygenfile:: seq_convolutions.h
    :project: Image convolution using MPI