.. _api_mpi_convolutions:

MPI convolutions
================

.. doxygenfile:: mpi_convolutions.h
    :project: Image convolution using MPI